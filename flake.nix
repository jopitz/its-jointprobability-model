{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    {
      overlays.default = (final: prev: {
        inherit (self.packages.${final.system}) its-jointprobability-model;
      });
    } //
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${ system};
          nix-filter = self.inputs.nix-filter.lib;

          its-jointprobability-model = pkgs.symlinkJoin {
            name = "its-jointprobability-model-0.1.0";
            paths = [
              (nix-filter {
                root = self;
                include = [
                  ./ProdSLDA_pyro.pt
                  ./ProdSLDA_state.pt
                  ./ProdSLDA_kwargs.pt
                ];
              })
            ];
          };
        in
        {
          packages = {
            inherit its-jointprobability-model;
            default = its-jointprobability-model;
          };
          devShells. default = pkgs.mkShell {
            buildInputs = [
              pkgs.git-lfs
            ];
          };
        });
}
